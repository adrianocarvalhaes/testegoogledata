/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.appengine.helloworld;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;




// [START example]
@SuppressWarnings("serial")
public class HelloServlet extends HttpServlet {
	
	
  @Override
  public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    PrintWriter out = resp.getWriter();
    out.println("Hello, world!!!");
    
    DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    
    Key key = KeyFactory.createKey("Trip", 4344);
    
    
    Entity trip = new Entity(key);
    trip.setProperty("latitudeEnd", "-19.865534");
    trip.setProperty("latitudeStart", "-19.933705");
    trip.setProperty("longitudeEnd", "-43.971142");
    trip.setProperty("longitudeStart", "-43.938533");
    trip.setProperty("title", "Teste Java");

    datastore.put(trip);
    
    out.println("gravou");
    
    Entity employee = new Entity("Employee", "asalieri");
    employee.setProperty("firstName", "Antonio");
    employee.setProperty("lastName", "Salieri");
    employee.setProperty("hireDate", new Date());
    employee.setProperty("attendedHrTraining", true);

    datastore.put(employee);
    
    out.println("gravou 2");
    
   
    
  }
}
// [END example]
